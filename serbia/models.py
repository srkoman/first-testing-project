from django.db import models
from django.contrib.gis.db.models import PointField

# Create your models here.


class FakeSerbia(models.Model):
    name = models.CharField(max_length=20)
    id = models.IntegerField(primary_key=True)
    location = models.CharField(max_length=20)
    geom = PointField(srid=4326)

    def __str__(self):
        return self.name
