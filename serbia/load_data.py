import os
from django.contrib.gis.utils import LayerMapping
from .models import FakeSerbia

serbia_mapping = {
    'name': 'name',
    'id': 'id',
    'location': 'location',
    'geom': 'Point',
}

shape = "https://sreckokuz.github.io/hosting-/rs-cities.geojson"


def run():
    lm = LayerMapping(FakeSerbia, shape, serbia_mapping)
    lm.save(verbose=True)
