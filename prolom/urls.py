from django.urls import path, include
from .views import EmployViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register('', EmployViewSet)

urlpatterns = [
    path('em/', include(router.urls)),
]
