from django.apps import AppConfig


class ProlomConfig(AppConfig):
    name = 'prolom'
