from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.views import APIView
from django.contrib.auth.models import User
from .serializers import EmploySerializer
# Create your views here.


class EmployViewSet(APIView):
    queryset = User.objects.all()
    serializer_class = EmploySerializer
