from .views import polls_list, polls_detail
from django.urls import path
urlpatterns = [
    path("poll/", polls_list, name="polls_list"),
    path("poll/<int:pk>/", polls_detail, name="polls_detail")
]
