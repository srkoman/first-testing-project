from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.core import serializers
from .models import Polygonion
from urllib.parse import urlparse
import json
from . forms import PolyForm
from django.contrib.gis.geos import GEOSGeometry
import os
from geo.settings import BASE_DIR
import logging
logging.basicConfig(
    filename=os.path.join(BASE_DIR, 'layers/static/layers/test.txt'), level=logging.DEBUG, format='%(asctime)s:%(levelname)s:%(message)s')


# Create your views here.


def display_map(request):
    form = PolyForm(request.POST or None)
    return render(request, 'layers/index.html', {'form': form})


@csrf_exempt
def saveElements(request):
    print("-----------------")
    name__ = request.POST['allFormData'].split('&')
    name_ = name__[0].split('=')
    name = str(name_[1])
    print(name)
    print('------------------')
    age__ = request.POST['allFormData'].split('&')
    age_ = age__[1].split('=')
    age = int(age_[1])
    print(age)
    print("-----------------------")

    geoJsonData = request.POST['geoJsonData']
    print(geoJsonData)
    print("-----------------------")

    q = json.loads(geoJsonData)

    print(q['features'][0]['geometry'])
    elType = GEOSGeometry(json.dumps(q['features'][0]['geometry']))
    print("-----------------------")
    print(elType)
    print("-----------------------")

    # LineType.objects.create(name=name, age=age, line=elType)

    return HttpResponse('OK')


def display_cities(request):
    return render(request, 'layers/cities.html')


def drawing_cities(request):
    return render(request, 'layers/drawing.html')


def error_handling(request):
    # try:
    #     f = open(os.path.join(BASE_DIR, 'layers/static/layers/test.txt'))
    #     print(f)
    # except FileNotFoundError as e:
    #     print(e)
    # except Exception as e:
    #     print(e)
    # else:
    #     print('OPEN')
    # finally:
    #     print('srkomanijak')
    p = 4+3
    q = 5+5
    logging.debug(p)
    logging.debug(q)

    return render(request, 'layers/error.html')
