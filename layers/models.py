from django.contrib.gis.db.models import PolygonField
from django.db import models
from rest_framework_gis.serializers import (GeoFeatureModelSerializer)

# Create your models here.


class Polygonion(models.Model):
    name = models.CharField(max_length=20)
    age = models.IntegerField()
    pol = PolygonField(srid=4269)

    def __str__(self):
        return self.name
