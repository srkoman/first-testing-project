function init() {
    const map = new ol.Map({
        view: new ol.View({
            center: [1821422.4655897599, 5678243.28108],
            zoom: 6
        }),
        target: 'cities-map'
    });

    let baseRasterLayer = new ol.layer.Tile({
        source: new ol.source.OSM()
    });

    map.addLayer(baseRasterLayer);
    //styling all features
    let stylingBasicVectorLayer = function (feature) {
        let style = [new ol.style.Style({
            image: new ol.style.Circle({
                fill: new ol.style.Fill({
                    color: '#aa80ff'
                }),
                stroke: new ol.style.Stroke({
                    color: ' #661aff',
                    width: 5
                }),
                radius: 6
            }),
            text: new ol.style.Text({
                text: String(feature.get('ID')),
                fill: new ol.style.Fill({
                    color: '#ff1a75'
                }),
                stroke: new ol.style.Stroke({
                    color: '#f2f2f2',
                    width: 4
                })
            })
        })
        ]
        return style;
    }

    const baseVectorLayer = new ol.layer.Vector({
        source: new ol.source.Vector({
            url: 'https://sreckokuz.github.io/hosting-/cities.geojson',
            format: new ol.format.GeoJSON()
        }),
        style: stylingBasicVectorLayer
    });

    //get DOM elements 

    //styling selected item
    mapView = map.getView();
    let select = new ol.interaction.Select();
    map.addInteraction(select);
    select.on('select', function (feature) {
        //console.log(feature.mapBrowserEvent.coordinate);
        //style
        console.log(typeof (feature.selected[0]));
        if (feature) {
            feature.selected[0].setStyle([
                new ol.style.Style({
                    image: new ol.style.Circle({
                        fill: new ol.style.Fill({
                            color: '#ffff4d'
                        }),
                        stroke: new ol.style.Stroke({
                            color: '#ff1a1a',
                            width: 5
                        }),
                        radius: 4
                    }),
                    text: new ol.style.Text({
                        text: String(feature.selected[0].get('ID')),
                        stroke: new ol.style.Stroke({
                            color: '#8cff1a',
                            width: 5
                        })
                    })
                })
            ])
        };
        //animate
        mapView.animate({ zoom: 7 }, { center: feature.mapBrowserEvent.coordinate })
        //change nav
        nameOfCity = feature.selected[0].get('name');
        let allNavigationElements = document.querySelector('.nav-menu');
        let activeNavigationElement = document.querySelector('.active');
        activeNavigationElement.className = activeNavigationElement.className.replace('active', '');
        console.log(allNavigationElements.children.namedItem(nameOfCity));
        let p = allNavigationElements.children.namedItem(nameOfCity).className = 'active';
    })

    let navMenu = document.querySelectorAll('.nav-menu');
    console.log(navMenu);
    for (nav of navMenu) {
        nav.addEventListener('click', function (e) {
            let nameOfPoint = e.target.id;
            console.log(e.target);
            let navElementToActiveSet = e.target;
            let activeNavigationElement = document.querySelector('.active');
            activeNavigationElement.className = activeNavigationElement.className.replace('active', '');
            navElementToActiveSet.className = 'active';

            let allFeturesFromLayer = baseVectorLayer.getSource().getFeatures();
            for (let feature of allFeturesFromLayer) {
                feature.setStyle(undefined);
                if (feature.get('name') === nameOfPoint) {
                    feature.setStyle([
                        new ol.style.Style({
                            image: new ol.style.Circle({
                                fill: new ol.style.Fill({
                                    color: '#ffff4d'
                                }),
                                stroke: new ol.style.Stroke({
                                    color: '#ff1a1a',
                                    width: 5
                                }),
                                radius: 4
                            }),
                            text: new ol.style.Text({
                                text: String(feature.get('ID')),
                                stroke: new ol.style.Stroke({
                                    color: '#8cff1a',
                                    width: 5
                                })
                            })
                        })
                    ]);
                    //console.log(feature);
                    mapView.animate({ zoom: 7 }, { center: feature.values_.geometry.flatCoordinates })
                } else if (nameOfPoint === 'Home') {
                    mapView.animate({ zoom: 6 }, { center: [1821422.4655897599, 5678243.28108] })
                }
            }

        });
    }

    popup = document.getElementById('srk');
    console.log(popup);
    const overlay = new ol.Overlay({
        element: popup
    });
    map.addOverlay(overlay);

    map.on('hover', function (e) {
        p = map.hasFeatureAtPixel(e.pixel);
        if (p) {
            feature = map.getFeaturesAtPixel(e.pixel);
            //console.log(feature[0].get('name'));
            popup.innerHTML = feature[0].get('name');
            overlay.setPosition(e.coordinate);
        } else {
            overlay.setPosition(undefined);

        }
    })



}

window.onload = init;