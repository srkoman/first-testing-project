function init() {
    const map = new ol.Map({
        view: new ol.View({
            center: [1821422.4655897599, 5678243.28108],
            zoom: 6
        }),
        target: 'cities-map'
    });

    let baseRasterLayer = new ol.layer.Tile({
        source: new ol.source.OSM()
    });
    map.addLayer(baseRasterLayer);

    let drawingLayer = new ol.interaction.Draw({
        type: 'Polygon'
    });

    map.addInteraction(drawingLayer);

    drawingLayer.on('drawend', function (e) {
        console.log(e.feature);
        let parser = new ol.format.GeoJSON();
        let parser2 = new ol.format.WKT();
        let drawenFeatures = parser.writeFeatures([e.feature]);
        let wkt = parser2.writeFeatures([e.feature]);
        drawenFeatures2 = new ol.format.WKT().readFeature(wkt);
        console.log(wkt);
        console.log(drawenFeatures);
        console.log(drawenFeatures2);
        var vector3 = new ol.layer.Vector({
            source: new ol.source.Vector({
                features: [drawenFeatures2]
            })
        });
        var modify = new ol.interaction.Modify({
            source: new ol.source.Vector({
                features: [drawenFeatures2]
            })
        });
        map.addLayer(vector3)
        map.addInteraction(modify);

    })




    var wkt = 'POLYGON((10.689 -25.092, 34.595 ' +
        '-20.170, 38.814 -35.639, 13.502 ' +
        '-39.155, 10.689 -25.092))';

    var feature = new ol.format.WKT().readFeature(wkt, {
        dataProjection: 'EPSG:4326',
        featureProjection: 'EPSG:3857'
    });
    var vector2 = new ol.layer.Vector({
        source: new ol.source.Vector({
            features: [feature]
        })
    });
    map.addLayer(vector2);

}

window.onload = init;