from rest_framework import serializers
from .models import Employ


class EmploySerializer(serializers.ModelSerializer):
    class Meta:
        model = Employ
        fields = ['name', 'age', 'second', 'url', 'year']
