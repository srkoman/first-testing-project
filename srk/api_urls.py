from django.urls import path, include
from rest_framework import routers
from .views import EmployViewSet

router = routers.SimpleRouter()
router.register('', EmployViewSet)
urlpatterns = [
    path('employ/', include(router.urls)),
]
